# Basic Backend

Use **nvm** ([Node Version Manager](https://github.com/nvm-sh/nvm)) to define node environment.

```sh
nvm install 14
nvm use 14
```

## Develop Environment

```sh
npm install
npm start
```

Going to [http://localhost:3000](http://localhost:3000) or [http://127.0.0.1:3000](http://127.0.0.1:3000).
